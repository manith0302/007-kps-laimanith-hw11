import React,{useState, useEffect} from 'react'; 

function SetCalculation() {

    let [inputNumber1, SetInput1] = useState('');
    let [inputNumber2, SetInput2] = useState('');

    let number1 = parseInt(inputNumber1);
    let number2 = parseInt(inputNumber2);
    let getSymbol;
    let getResult;
    let getValueNumber;
    let getUniqueSymbol;

    let [addSymbol,SetSymbol] = useState(["Please choose (+,-,*,/) to calculate","Add '+'", "Subtract '-'", "Multiply '*'", "Divide '/'", "Modulus '%'"])
    let Add = addSymbol.map(Add => Add)

    let handleAddrTypeChange = (props) =>{
        if ((addSymbol[props.target.value]) === "Add '+'"){
            getResult = number1 + number2;
            getSymbol = "+";
            getValueNumber = inputNumber1 + getSymbol + inputNumber2;
            getUniqueSymbol = addSymbol[props.target.value];
        }
        else if ((addSymbol[props.target.value]) === "Subtract '-'"){
            getResult = number1 - number2;
            getSymbol = "-";
            getValueNumber = inputNumber1 + getSymbol + inputNumber2;
            getUniqueSymbol = addSymbol[props.target.value];
        }
        else if ((addSymbol[props.target.value]) === "Multiply '*'"){
            getResult = number1 * number2;
            getSymbol = "*";
            getValueNumber = inputNumber1 + getSymbol + inputNumber2;
            getUniqueSymbol = addSymbol[props.target.value];
        }
        else if ((addSymbol[props.target.value]) === "Divide '/'"){
            getResult = (number1 / number2).toFixed(4);
            getSymbol = "/";
            getValueNumber = inputNumber1 + getSymbol + inputNumber2;
            getUniqueSymbol = addSymbol[props.target.value];
        }
        else if ((addSymbol[props.target.value]) === "Modulus '%'"){
            getResult = number1 % number2;
            getSymbol = "%";
            getValueNumber = inputNumber1 + getSymbol + inputNumber2;
            getUniqueSymbol = addSymbol[props.target.value];
        }
    }
    // const peopleLis = for(var i = 0; i < people.length; i++){
    //     return <li>{people[i]}</li>
    // }
    
    let [getNumbers, SetNumbers] = useState([])

    let Calculate = () => {

        getValueNumber = inputNumber1 + getSymbol + inputNumber2;

        SetInput1('')
        SetInput2('')

        console.log(`Add Result ${Add.value}`);
        console.log(`Get Value Number ${getValueNumber}`);
        console.log(`Get Unique Symbol ${getUniqueSymbol}`);
        console.log(`Get Result ${getResult}`);

        if(getValueNumber !== getSymbol && getResult !== undefined) {
            SetNumbers([ ... getNumbers, {
                id : getNumbers.length,
                value : getResult,
                valueNumber : getValueNumber
            }]);
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-6">
                    <div className="row">
                        <div className="col-12">
                            <img src="https://stlawyers.ca/wp-content/uploads/2016/11/Calculate-Severance-Pay.gif" alt=""/>
                        </div>
                        <div className="col-12">
                            <form action="">
                                <input value={inputNumber1} type="number" onChange={inputNumber1 => SetInput1(inputNumber1.target.value)} placeholder="Please input number"/>
                                <input value={inputNumber2} type="number" onChange={inputNumber2 => SetInput2(inputNumber2.target.value)} placeholder="Please input number"/>
                            </form>
                        </div>
                        <div className="col-12">
                            <select
                                key = "1"
                                onChange={props => handleAddrTypeChange(props)}
                                className="browser-default custom-select" >
                                {
                                    Add.map((symbol, key) => <option value={key} key={key}>{symbol}</option>)
                                }
                            </select>
                        </div>
                        <div className="col-12">
                            <button className="btn btn-primary" onClick={Calculate}>Calculate</button> 
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <ul>
                        {getNumbers.map(number => (
                            <li key={number.id} className ="number-result">{number.valueNumber} = {number.value}</li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SetCalculation;
