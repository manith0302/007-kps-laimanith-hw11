import React from 'react';
import './App.css';

import SetCalculation from './Components/SetCalculation';

function App() {

  return (
    <div>
      <SetCalculation />
    </div>
  );
}
export default App;
